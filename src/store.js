import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    paymentSteps: [
      {name:"payer",status:false},
      {name:"reports",status:false},
      {name:"analysis",status:false},
      {name:"documents",status:false},
      {name:"summary",status:false}
    ],
    selectedRoute: 'Mouth',
    selectedOrigin: '',
    reports: [],
    analysisReport: [
      {link:"https://www.google.com/",title:"Admission Assessment ",text:"quality of baseline and documentation",id:"styled-checkbox-1"},
      {link:"https://www.google.com/",title:"Lorem Ipsum ",text:"lorem ipsum lorem ipsum lorem ipsum",id:"styled-checkbox-2"},
      {link:"qhttps://www.google.com/",title:"Plan of Care ",text:"quality of Care / following the plan",id:"styled-checkbox-3"},
      {link:"https://www.google.com/",title:"Plan of Care ",text:"cross functional team involvement in creation of P.O.C",id:"styled-checkbox-4"},
      {link:"https://www.google.com/",title:"Hospice Eligibility",text:"patient is eligible for services",id:"styled-checkbox-5"},
      {link:"https://www.google.com/",title:"Therapy / Skillable",text:"patient is eligible and requires skilled care",id:"styled-checkbox-6"},
    ],
  },
  getters: {
    selectedOrigin: state => {
        return state.selectedOrigin
      },
    	analysisReport: state => {
        	return state.analysisReport
        },
      getReports: state => {
          return state.reports
        },
        selectedRoute: state => {
            return state.selectedRoute
          },
  },
  mutations: {
    addReport(state, payload) {
        state.reports.push(payload)
},
updateselectedOrigin(state, payload) {
    state.selectedOrigin = payload
},
removeReport(state,  payload) {
  console.log(payload)
        state.reports.splice(payload, 1)
    },
  },
  actions: {
    updateselectedOrigin({ commit }, payload) {
        commit('updateselectedOrigin', payload);
      },
      addReport({ commit }, payload) {
          commit('addReport', payload);
        },
        updateSelectedRoute({ commit }, payload) {
            commit('updateSelectedRoute', payload);
          },
          updateSelectedRoute({ commit }, payload) {
              commit('updateSelectedRoute', payload);
            },

      removeReport({ commit, state, dispatch }, payload) {
        console.log(payload)
				const reportItem = state.reports.find(item => item === payload)
				const reportObj = state.reports.indexOf(reportItem)
					commit('removeReport', reportObj)
			},

  }
})
