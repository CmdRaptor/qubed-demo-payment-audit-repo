import Vue from 'vue'
import Router from 'vue-router'
import paymentAudit from './views/paymentAudit.vue'
import paymentAuditPage2 from './views/paymentAuditPage2.vue'
import paymentAuditPage3 from './views/paymentAuditPage3.vue'
import paymentAuditPage4 from './views/paymentAuditPage4.vue'
import paymentAuditPage5 from './views/paymentAuditPage5.vue'
import paymentAuditPage6 from './views/paymentAuditPage6.vue'
import paymentAuditPage7 from './views/paymentAuditPage7.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/paymentAudit'
    },
    {
      path: '/paymentAudit',
      name: 'paymentAudit',
      component: paymentAudit
    },
    {
      path: '/paymentAuditPage2',
      name: 'paymentAuditPage2',
      component: paymentAuditPage2
    },
    {
      path: '/paymentAuditPage3',
      name: 'paymentAuditPage3',
      component: paymentAuditPage3
    },
    {
      path: '/paymentAuditPage4',
      name: 'paymentAuditPage4',
      component: paymentAuditPage4
    },
    {
      path: '/paymentAuditPage5',
      name: 'paymentAuditPage5',
      component: paymentAuditPage5
    },
    {
      path: '/paymentAuditPage6',
      name: 'paymentAuditPage6',
      component: paymentAuditPage6
    },
    {
      path: '/paymentAuditPage7',
      name: 'paymentAuditPage7',
      component: paymentAuditPage7
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: function () { 
        return import(/* webpackChunkName: "about" */ './views/About.vue')
      }
    }
  ]
})
